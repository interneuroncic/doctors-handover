import { Component, OnInit, OnDestroy } from "@angular/core";
//import { SubjectsService } from './services/subjects.service';
import { Subscription } from "rxjs";
import { ApirequestService } from "../services/apirequest.service";
import { AppService } from "../services/app.service";
//import { EscalationForm } from '../models/fluidbalanceescalation.model';
import { v4 as uuidv4 } from "uuid";
import { SubjectsService } from "../services/subjects.service";
import { Person } from "../models/entities/core-person.model";
import { NgxSpinnerService } from "ngx-spinner";



@Component({
  selector: "app-viewer",
  templateUrl: "./viewer.component.html",
  styleUrls: ["./viewer.component.css"],
})
export class ViewerComponent implements OnInit {
  subscriptions: Subscription = new Subscription();

  person: Person = new Person();

  constructor(
    private apiRequest: ApirequestService,
    public appService: AppService,
    private subjects: SubjectsService,
    private spinner: NgxSpinnerService,
    private spinner2: NgxSpinnerService
  ) {

    
    /** spinner starts on init */
    this.spinner.show("sp1");

    this.subjects.personIdChange.subscribe(() => {
      this.subscriptions.add(
        this.apiRequest
          .getRequest(
            this.appService.baseURI +
              "/GetObject?synapsenamespace=core&synapseentityname=person&id=" +
              this.appService.personId
          )
          .subscribe((response) => {
            var data = JSON.parse(response);
            console.log(data);
            this.person = data;
            this.spinner.hide("sp1");

            this.spinner2.show("sp2");

            setTimeout(() => {
              /** spinner ends after 5 seconds */
              this.spinner2.hide("sp2");
            }, 5000);

          })

          //{"personpatientlist_id" : "3|d91ef1fa-e9c0-45ba-9e92-1e1c4fd468a2", "person_id" : "d91ef1fa-e9c0-45ba-9e92-1e1c4fd468a2", "patientlist_id" : "3"}
      );
   
   
      this.subscriptions.add(
        this.apiRequest
        .getRequest(
          this.appService.baseURI +
            "/GetObject?synapsenamespace=core&synapseentityname=person&id=" +
            this.appService.personId
        )
          .subscribe((response) => {
            var data = JSON.parse(response);
            console.log(data);
            this.person = data;
            this.spinner.hide("sp1");

            this.spinner2.show("sp2");

            setTimeout(() => {
              /** spinner ends after 5 seconds */
              this.spinner2.hide("sp2");
            }, 5000);

          })

          //{"personpatientlist_id" : "3|d91ef1fa-e9c0-45ba-9e92-1e1c4fd468a2", "person_id" : "d91ef1fa-e9c0-45ba-9e92-1e1c4fd468a2", "patientlist_id" : "3"}
      );
   
    });
  }

  ngOnInit() {}
}
