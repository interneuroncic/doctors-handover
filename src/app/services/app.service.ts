import { Injectable } from '@angular/core';
import { Encounter } from '../models/encounter.model';
import { ConfigModel } from '../models/config.model';
import { action } from '../models/filter.model';
import * as jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class AppService {

  public personId: string;
  public encounter: Encounter;
  public isCurrentEncouner = false;
  public apiService: any;
  public baseURI: string;
  public appConfig = new ConfigModel();
  public loggedInUserName: string = null;
  public enableLogging = true;
  public roleActions: action[] = [];

  
  constructor() { }

  logToConsole(msg: any) {
    if (this.enableLogging) {
      //this.appService.logToConsole(msg);
    }
  }

  decodeAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    }
    catch (Error) {
      return null;
    }
  }


}
