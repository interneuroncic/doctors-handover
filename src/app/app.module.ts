import { BrowserModule } from "@angular/platform-browser";
import {
  DoBootstrap,
  Injector,
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
} from "@angular/core";

import { AppComponent } from "./app.component";

import { createCustomElement } from "@angular/elements";
import { ViewerComponent } from "./viewer/viewer.component";

import { ModalModule, BsModalRef, BsModalService } from "ngx-bootstrap/modal";

import { HttpClientModule } from "@angular/common/http";

//import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

// Import library module
import { NgxSpinnerModule, NgxSpinnerService } from "ngx-spinner";

import {EditorModule} from 'primeng/editor';

import {FormsModule} from '@angular/forms';
import { HandoverformComponent } from './handoverform/handoverform.component';
import { TasklistComponent } from './tasklist/tasklist.component';
import { TaskformnewComponent } from './taskformnew/taskformnew.component';
import { TaskformviewComponent } from './taskformview/taskformview.component';


@NgModule({
  declarations: [AppComponent, ViewerComponent, HandoverformComponent, TasklistComponent, TaskformnewComponent, TaskformviewComponent],
  imports: [
    BrowserModule,
    ModalModule.forRoot(),
    HttpClientModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    EditorModule,
    FormsModule
  ],
  providers: [BsModalRef, BsModalService, NgxSpinnerService],
  bootstrap: [AppComponent], //Comment out when running build command for packaging

  entryComponents: [AppComponent, TaskformnewComponent, TaskformviewComponent],
})
export class AppModule implements DoBootstrap {
  constructor(private injector: Injector) {}

  ngDoBootstrap() {
    const el = createCustomElement(AppComponent, { injector: this.injector });
    customElements.define("app-doctors-handover", el); //Must Be unique - Gets passed to Sudio
  }
}
