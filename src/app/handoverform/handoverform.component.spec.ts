import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandoverformComponent } from './handoverform.component';

describe('HandoverformComponent', () => {
  let component: HandoverformComponent;
  let fixture: ComponentFixture<HandoverformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandoverformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandoverformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
