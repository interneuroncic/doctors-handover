import { Component, ElementRef, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { DoctorsHandover } from '../models/entities/doctorshandover.model';
import { ApirequestService } from '../services/apirequest.service';
import { AppService } from '../services/app.service';
import { v4 as uuidv4 } from "uuid";
import { SubjectsService } from '../services/subjects.service';
import { NgxSpinnerService } from "ngx-spinner";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-handoverform',
  templateUrl: './handoverform.component.html',
  styleUrls: ['./handoverform.component.css']
})
export class HandoverformComponent implements OnInit, OnDestroy {

  operation: string = "";
  diagnosisproblemshistory: string = "";
  resultsandreports: string = "";
  imaging: string = "";
  drainsandcatheters: string = "";
  wound: string = "";
  bloodsandinvestigations: string = "";
  plansandjobs: string = "";
  
  @ViewChild('openTemplate', {static: false}) openTemplateModal: ElementRef;

  modalRef: BsModalRef;

  subscriptions: Subscription = new Subscription();

  constructor(private apiRequestService: ApirequestService, private appService: AppService, private subjects: SubjectsService, private spinner: NgxSpinnerService, private modalService: BsModalService) {
    this.subscriptions.add(
      this.subjects.personIdChange.subscribe(
        (response) => {
          this.getDoctorsHandover();
        }
      )
    ); 
   }

  ngOnInit() {

  }

  ngOnDestroy() {
    
    this.subscriptions.unsubscribe();
  }

  saveDoctorsHandover(){
    let doctorsHandover = new DoctorsHandover();
    
    doctorsHandover.handover_doctorshandover_id = this.appService.personId;
    doctorsHandover.person_id = this.appService.personId;
    doctorsHandover.operation = this.operation;
    doctorsHandover.diagnosisproblemshistory = this.diagnosisproblemshistory;
    doctorsHandover.resultsandreports = this.resultsandreports;
    doctorsHandover.imaging = this.imaging;
    doctorsHandover.drainsandcatheters = this.drainsandcatheters;
    doctorsHandover.wound = this.wound;
    doctorsHandover.bloodsandinvestigations = this.bloodsandinvestigations;
    doctorsHandover.plansandjobs = this.plansandjobs;

    this.subscriptions.add(
      this.apiRequestService.postRequest(this.appService.baseURI + "/PostObject?synapsenamespace=local&synapseentityname=handover_doctorshandover", JSON.stringify(doctorsHandover))
      .subscribe(
        (response) => {
          this.openTemplateModal.nativeElement.click();
          this.subjects.frameworkEvent.next("UPDATE_EWS");
        }
      )
    );
  }

  getDoctorsHandover(){    
    this.subscriptions.add(
      this.apiRequestService.getRequest(this.appService.baseURI + "/GetListByAttribute?synapsenamespace=local&synapseentityname=handover_doctorshandover&synapseattributename=handover_doctorshandover_id&attributevalue=" + this.appService.personId)
      .subscribe(
        (response) => {
          let doctorsHandovers:  Array<DoctorsHandover> = [];
          doctorsHandovers = JSON.parse(response);
          //console.log(doctorsHandovers)
          
          let doctorsHandover = new DoctorsHandover();
          doctorsHandover = doctorsHandovers[0];
          //console.log(doctorsHandover);
          
          this.operation = doctorsHandover.operation;
          this.diagnosisproblemshistory = doctorsHandover.diagnosisproblemshistory;
          this.resultsandreports = doctorsHandover.resultsandreports;
          this.imaging = doctorsHandover.imaging;
          this.drainsandcatheters = doctorsHandover.drainsandcatheters;
          this.wound = doctorsHandover.wound;
          this.bloodsandinvestigations = doctorsHandover.bloodsandinvestigations;
          this.plansandjobs = doctorsHandover.plansandjobs;
        }        
      )
    );
  }

  public tab = { 
    key:9,
    handler: function() {
        return false;
    }
  };

  editorcall(event:any){
  event.editor.keyboard.bindings[9].length = 0;
  event.editor.keyboard.bindings[9].push(this.tab);
  }

  openModal(template: TemplateRef<any>) {
    const config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-dialog-centered',
      initialState: {

      }
    }

    this.modalRef = this.modalService.show(template, config);
  }
  
}
