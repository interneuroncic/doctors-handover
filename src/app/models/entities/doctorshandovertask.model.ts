export class DoctorsHandoverTask{
    public handover_doctorshandovertask_id: string;
    public handover_doctorshandover_id: string;
    public task: string;
    public assignedto: string;
    public status: string;
    public tasknote: string;
}