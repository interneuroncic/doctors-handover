export class DoctorsHandover{
    public handover_doctorshandover_id: string;
    public person_id: string;
    public operation: string;
    public diagnosisproblemshistory: string;
    public resultsandreports: string;
    public imaging: string;
    public drainsandcatheters: string;
    public wound: string;
    public bloodsandinvestigations: string;
    public plansandjobs: string;
}