export class Uris {
    public baseuri: string;
    public carerecordbaseuri: string;
  }
  
  export class Appsettings {
    sessionStartTime: number;
    maxIntakeRoutes: number;
    maxOutputRoutes: number;
  }
  
  export class ConfigModel {
    constructor(
      public uris?: Uris,
      public enablelogging?: boolean,
      public appsettings: Appsettings = new Appsettings(),
      public prodbuild?: boolean
    ) { };
  }