import { Component, OnInit } from '@angular/core';
import { TaskformnewComponent } from '../taskformnew/taskformnew.component';
import { TaskformviewComponent } from '../taskformview/taskformview.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AppService } from '../services/app.service';
import { ApirequestService } from '../services/apirequest.service';
import { Subscription } from 'rxjs';
import { DoctorsHandoverTask } from '../models/entities/doctorshandovertask.model';
import { SubjectsService } from '../services/subjects.service';



@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css']
})
export class TasklistComponent implements OnInit {

  doctorsHandoverTasks:  Array<DoctorsHandoverTask> = [];

  bsModalRef: BsModalRef;

  subscriptions: Subscription = new Subscription();

  constructor(private modalService: BsModalService, private appService: AppService, private apiRequestService: ApirequestService, private subjects: SubjectsService) { 
    this.subscriptions.add(
      this.subjects.personIdChange.subscribe(
        (response) => {
          this.getDoctorsHandoverTasks();
        }
      )
    );

    this.subscriptions.add(
      this.subjects.taskChange.subscribe(
        (response) => {
          this.getDoctorsHandoverTasks();
        }
      )
    );
  }

  ngOnInit() {
        
  }

  openAddTaskForm() {
    const config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-dialog-centered modal-lg',
      initialState: {
        // selectedRouteTypeText: "Please select",
        // selectedRouteText: "Please select",
        // selectedRouteId: "",
        // ioRoutes: [],
        // showErrorMessage: false,
        // errorMessage: ""
      }
    }

    this.bsModalRef = this.modalService.show(TaskformnewComponent, config);
    
  }

  openViewEditTaskForm(doctorsHandoverTaskId: string) {
    const config = {
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-dialog-centered modal-lg',
      initialState: {
         doctorsHandoverTaskId: doctorsHandoverTaskId,
        // selectedRouteText: "Please select",
        // selectedRouteId: "",
        // ioRoutes: [],
        // showErrorMessage: false,
        // errorMessage: ""
      }
    }

    this.bsModalRef = this.modalService.show(TaskformviewComponent, config);
    
  }

  getDoctorsHandoverTasks(){
    this.subscriptions.add(
      this.apiRequestService.getRequest(this.appService.baseURI + "/GetListByAttribute?synapsenamespace=local&synapseentityname=handover_doctorshandovertask&synapseattributename=handover_doctorshandover_id&attributevalue=" + this.appService.personId + "&orderby=_sequenceid")
      .subscribe(
        (response) => {
          this.doctorsHandoverTasks = JSON.parse(response);
          this.doctorsHandoverTasks.reverse();
          //console.log(this.doctorsHandoverTasks)
          

          
        }
      )
    );
  }

}
