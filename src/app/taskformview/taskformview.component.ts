import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { DoctorsHandoverTask } from '../models/entities/doctorshandovertask.model';
import { ApirequestService } from '../services/apirequest.service';
import { AppService } from '../services/app.service';
import { SubjectsService } from '../services/subjects.service';

@Component({
  selector: 'app-taskformview',
  templateUrl: './taskformview.component.html',
  styleUrls: ['./taskformview.component.css']
})
export class TaskformviewComponent implements OnInit {

  task: string = "";
  assignedto: string = "";
  status: string = "";
  tasknote: string = "";
  doctorsHandoverTaskId: string = "";

  subscriptions: Subscription = new Subscription();

  constructor(public bsModalRef: BsModalRef, private modalService: BsModalService, private appService: AppService, private apiRequestService: ApirequestService, private subjects: SubjectsService) { }

  ngOnInit() {
    this.getDoctorsHandoverTask(this.doctorsHandoverTaskId);
  }

  getDoctorsHandoverTask(handover_doctorshandovertask_id: string){
    this.subscriptions.add(
      this.apiRequestService.getRequest(this.appService.baseURI + "/GetListByAttribute?synapsenamespace=local&synapseentityname=handover_doctorshandovertask&synapseattributename=handover_doctorshandovertask_id&attributevalue=" + handover_doctorshandovertask_id)
      .subscribe(
        (response) => {
          let doctorsHandoverTasks:  Array<DoctorsHandoverTask> = [];
          doctorsHandoverTasks = JSON.parse(response);
          //console.log(doctorsHandoverTasks)
          
          let doctorsHandoverTask = new DoctorsHandoverTask();
          doctorsHandoverTask = doctorsHandoverTasks[0];
          //console.log(doctorsHandoverTask);
          
          this.task = doctorsHandoverTask.task;
          this.assignedto = doctorsHandoverTask.assignedto;
          this.status = doctorsHandoverTask.status;
          this.tasknote = doctorsHandoverTask.tasknote;
          
        }
      )
    );
  }

  updateTask(){
    let doctorsHandoverTask = new DoctorsHandoverTask();
    doctorsHandoverTask.handover_doctorshandovertask_id = this.doctorsHandoverTaskId;
    doctorsHandoverTask.handover_doctorshandover_id = this.appService.personId;
    doctorsHandoverTask.task = this.task;
    doctorsHandoverTask.assignedto = this.assignedto;
    doctorsHandoverTask.status = this.status;
    doctorsHandoverTask.tasknote = this.tasknote;

    this.subscriptions.add(
      this.apiRequestService.postRequest(this.appService.baseURI + "/PostObject?synapsenamespace=local&synapseentityname=handover_doctorshandovertask", JSON.stringify(doctorsHandoverTask))
      .subscribe(
        (response) => {
          this.bsModalRef.hide();
          this.subjects.taskChange.next();
          this.subjects.frameworkEvent.next("UPDATE_EWS");
        }
      )
    );
  }
}
