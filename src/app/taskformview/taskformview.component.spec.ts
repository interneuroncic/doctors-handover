import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskformviewComponent } from './taskformview.component';

describe('TaskformviewComponent', () => {
  let component: TaskformviewComponent;
  let fixture: ComponentFixture<TaskformviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskformviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskformviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
