import { Component, OnDestroy, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DoctorsHandoverTask } from '../models/entities/doctorshandovertask.model';
import { v4 as uuidv4 } from "uuid";
import { AppService } from '../services/app.service';
import { Subscription } from 'rxjs';
import { ApirequestService } from '../services/apirequest.service';
import { SubjectsService } from '../services/subjects.service';

@Component({
  selector: 'app-taskformnew',
  templateUrl: './taskformnew.component.html',
  styleUrls: ['./taskformnew.component.css']
})
export class TaskformnewComponent implements OnInit, OnDestroy {

  task: string = "";
  assignedto: string = "";
  status: string = "New";
  tasknote: string = "";

  subscriptions: Subscription = new Subscription();

  constructor(public bsModalRef: BsModalRef, private modalService: BsModalService, private appService: AppService, private apiRequestService: ApirequestService, private subjects: SubjectsService) { }

  ngOnInit() {
  }

  createNewTask(){
    let doctorsHandoverTask = new DoctorsHandoverTask();
    doctorsHandoverTask.handover_doctorshandovertask_id = uuidv4();
    doctorsHandoverTask.handover_doctorshandover_id = this.appService.personId;
    doctorsHandoverTask.task = this.task;
    doctorsHandoverTask.assignedto = this.assignedto;
    doctorsHandoverTask.status = this.status;
    doctorsHandoverTask.tasknote = this.tasknote;

    this.subscriptions.add(
      this.apiRequestService.postRequest(this.appService.baseURI + "/PostObject?synapsenamespace=local&synapseentityname=handover_doctorshandovertask", JSON.stringify(doctorsHandoverTask))
      .subscribe(
        (response) => {
          this.bsModalRef.hide();
          this.subjects.taskChange.next();
          this.subjects.frameworkEvent.next("UPDATE_EWS");
        }
      )
    );
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
}
