import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskformnewComponent } from './taskformnew.component';

describe('TaskformnewComponent', () => {
  let component: TaskformnewComponent;
  let fixture: ComponentFixture<TaskformnewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaskformnewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskformnewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
