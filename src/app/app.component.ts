import { Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import { isArray } from 'util';
import { action, DataContract, filter, filterparam, filterParams, filters, orderbystatement, selectstatement } from './models/filter.model';
import { ApirequestService } from './services/apirequest.service';
import { AppService } from './services/app.service';
import { SubjectsService } from './services/subjects.service';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';


// Import library module
import { NgxSpinnerModule } from "ngx-spinner";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {

  title = 'doctors-handover';
  subscriptions: Subscription = new Subscription();
  //show: boolean = false;
  @Input() set datacontract(value: DataContract) {
    this.appService.personId = value.personId;
    this.appService.apiService = value.apiService;
    this.subjects.unload = value.unload;
    this.initConfigAndGetMeta(this.appService.apiService)
  }
  @Output() frameworkAction = new EventEmitter<string>();

  constructor(private subjects: SubjectsService, public appService: AppService, private apiRequest: ApirequestService, private modalService: BsModalService) {

    this.subscriptions.add(this.subjects.frameworkEvent.subscribe((e: string) => { this.emitFrameworkEvent(e) }
    ));

    console.log(environment.production);
    if (!environment.production)
      this.initDevMode();
  }

  emitFrameworkEvent(e: string) {    
    this.frameworkAction.emit(e);
  }

  initDevMode() {
    //commment out to push to framework - 3lines
    this.appService.personId =  "17775da9-8e71-4a3f-9042-4cdcbf97efec";// "0422d1d0-a9d2-426a-b0b2-d21441e2f045";//"429904ca-19c1-4a3a-b453-617c7db513a3";//"027c3400-24cd-45c1-9e3d-0f4475336394";//"429904ca-19c1-4a3a-b453-617c7db513a3";

    let value: any = {};
    value.authService = {};
    value.authService.user = {};
    let auth = this.apiRequest.authService;
    auth.getToken().then((token) => {
      value.authService.user.access_token = token;
      this.initConfigAndGetMeta(value);
    });

  }


  async initConfigAndGetMeta(value: any) {
 
    this.appService.apiService = value;
    let decodedToken: any;
    if (this.appService.apiService) {
      decodedToken = this.appService.decodeAccessToken(this.appService.apiService.authService.user.access_token);
      if (decodedToken != null)
        this.appService.loggedInUserName = decodedToken.name ? (isArray(decodedToken.name) ? decodedToken.name[0] : decodedToken.name) : decodedToken.IPUId;
 
    }
    await this.subscriptions.add(this.apiRequest.getRequest("./assets/config/doctorshandoverconfig.json?V" + Math.random()).subscribe(
      async (response) => {
        this.appService.appConfig = response;
        this.appService.baseURI = this.appService.appConfig.uris.baseuri;
        //console.log(this.appService.appConfig.uris.baseuri);
        this.appService.enableLogging = this.appService.appConfig.enablelogging;
 
       //this.show = true;
        //get actions for rbac
        // await this.subscriptions.add(this.apiRequest.postRequest(`${this.appService.baseURI}/GetBaseViewListByPost/rbac_actions`, this.createRoleFilter(decodedToken))
        //   .subscribe((response: action[]) => {
        //     this.appService.roleActions = response;
        //     this.appService.logToConsole(response);
        //   }));
 
       // await this.getMetaData();
 
        //get all meta before emitting events
        //all components depending on meta should perform any action only after receiveing these events
        //use await on requets that are mandatory before the below events can be fired.
 
        //emit events after getting initial config. //this happens on first load only. 
        this.appService.logToConsole("Service reference is being published from init config");
        this.subjects.apiServiceReferenceChange.next();
        this.appService.logToConsole("personid is being published from init config");
        this.subjects.personIdChange.next();

        
 
      }));
 
  }
 
  createRoleFilter(decodedToken: any) {

    let condition = "";
    let pm = new filterParams();
    let synapseroles;
    if (environment.production)
      synapseroles = decodedToken.SynapseRoles
    else
      synapseroles = decodedToken.client_SynapseRoles
    if (!isArray(synapseroles)) {
      condition = "rolename = @rolename";
      pm.filterparams.push(new filterparam("rolename", synapseroles));
    }
    else
      for (var i = 0; i < synapseroles.length; i++) {
        condition += "or rolename = @rolename" + i + " ";
        pm.filterparams.push(new filterparam("rolename" + i, synapseroles[i]));
      }
    condition = condition.replace(/^\or+|\or+$/g, '');
    let f = new filters();
    f.filters.push(new filter(condition));


    let select = new selectstatement("SELECT *");

    let orderby = new orderbystatement("ORDER BY 1");

    let body = [];
    body.push(f);
    body.push(pm);
    body.push(select);
    body.push(orderby);

    this.appService.logToConsole(JSON.stringify(body));
    return JSON.stringify(body);
  }

  ngOnDestroy(){
    this.subjects.unload.next("app-doctors-handover");
  }

  
}
